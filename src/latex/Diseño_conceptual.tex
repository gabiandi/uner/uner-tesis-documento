\chapter{Diseño conceptual de la solución}

\pagestyle{empty}

\newpage

\pagestyle{fancy}

\section{Justificación técnica}

\subsection{Industria 4.0}

La Cuarta Revolución Industrial, o Industria 4.0, conceptualiza el cambio rápido en la tecnología, las industrias, los patrones y procesos sociales en el siglo XXI debido a la creciente interconectividad y la automatización inteligente. Esto esta produciendo cambios fundamentales en la forma en que opera la red global de producción y suministro a través de la automatización continua de las prácticas industriales y de fabricación tradicionales. El uso de máquinas inteligentes permite analizar y diagnosticar problemas sin necesidad de intervención humana.

\begin{center}
    \includegraphics[scale=0.6]{src/imagenes/ingenieria/industria.jpg}
\end{center}

La Industria 4.0 implica la promesa de una nueva revolución que combina técnicas avanzadas de producción y operaciones con tecnologías inteligentes que se integrarán en las organizaciones, las personas y los activos. Se refiere a una nueva manera de producir mediante la adopción de tecnologías 4.0, es decir, de soluciones enfocadas en la interconectividad, la automatización y los datos en tiempo real.

Esta transformación no solo abarca a la producción de bienes y/o servicios de la empresa, sino a toda la cadena de valor, dado que reconfigura tanto los procesos de elaboración y las prestaciones de productos, como la gestión empresaria, las relaciones clientes y proveedores y, en un sentido más amplio, los modelos de negocios. \footnote{Definiciones extraídas de: \href{https://es.wikipedia.org/wiki/Cuarta\_Revoluci\%C3\%B3n\_Industrial}{Wikipedia}}

\subsection{Del SCADA al IoT}

Hasta hace poco, los sistemas IT (Tecnologías de la información) servían para relacionar los datos con personas, mientras que los sistemas OT (Tecnologías de la operación) se utilizaban para relacionar las personas con los dispositivos electromecánicos. Con la eclosión del IoT, este paradigma ha cambiado por completo. Ahora las grandes empresas del mundo IT quieren interactuar con los dispositivos físicos y están ofreciendo soluciones que en muchos aspectos compiten con la oferta tradicional de los sistemas SCADA. Los grandes actores del sector están empezando a ofrecer plataformas de integración para dispositivos a través de internet. Al mismo tiempo, las empresas más tradicionales del mundo OT han reaccionado rápidamente para no perder su posicionamiento y empresas como GE, Schneider o Siemens empiezan a ofrecer también sus plataformas. Sin embargo, mantienen su oferta clásica de sistemas SCADA, que todavía se encuentra bastante desvinculada de estas nuevas plataformas.

\subsection{Sistemas de control para industria}

En cuanto a sistemas de control para industria, existen multitud de variantes y alternativas que elegir. El Controlador Lógico Programable (PLC) es la opción más utilizada debido a su estandarización y robustez.

También podemos encontrar computadoras destinadas a fines industriales, con un factor de forma entre un nettop y un rack de servidores. Las PC industriales tienen estándares más altos de confiabilidad y precisión que una computadora personal para uso en oficina.

Ademas, existen sensores completos en donde se tiene una electrónica de toma de datos y ajuste. Esto presenta una ventaja en cuando a la simplicidad de componentes necesarios para llevar a cabo el control sobre una variable del sistema.

Por otro lado, se puede optar por un sistema de control diseñado desde cero. Diseñar la electrónica y el Software tiene una serie de ventajas. Dentro de ellas la más destacable es la \textbf{flexibilidad}. Al utilizar tecnologías libres, toda su documentación y desarrollo está disponible para que cualquier persona pueda leer o modificar lo que quiera. Esto también implica que su utilización no esté limitada por licencias privativas.

Este tipo de sistemas al ser un desarrollo propio se pueden personalizar tanto como se quisiera para poder suplir las necesidades requeridas, lo que es una ventaja respecto a los controladores estandarizados. Sin embargo, no todo es color de rosas, el emplear tecnologías Open Source y Open Hard, a menudo imponen que todo el desarrollo también sea libre. En procesos industriales esto no es un impedimento, ya que a menudo no todas las empresas tienen una planta de tratamiento de efluentes con exactamente los mismos requerimientos. Para fines comerciales, el valor agregado al proceso no tiene que ver con el desarrollo de las tecnologías aplicadas en él, sino con el producto fruto de ellas.

Para presentar un análisis resumido se muestra la siguiente tabla comparativa:

\begin{table}[H]
    \begin{center}
        \begin{tabular}{|c|c|c|c|}
            \hline
            & \textbf{PLC} & \textbf{IPC} & \textbf{Desarrollo propio} \\ \hline
            \textbf{Entras y salidas} & Si & Si & Si \\ \hline
            \textbf{Expansión} & Si & Si & Si \\ \hline
            \textbf{Estándar} & Si & Si & No \\ \hline
            \textbf{Protecciones} & Si & Si & Si \\ \hline
            \textbf{Libre uso} & No & No & Si \\ \hline
            \textbf{Garantía} & Oficial & Oficial & No oficial \\ \hline
        \end{tabular}
    \end{center}
\end{table}

\section{Modificaciones al proceso actual}

Incorporar un nuevo sistema de control al proceso que está actualmente operando en la empresa, requerirá realizar ciertas modificaciones. Estas modificaciones tienen como premisa principal, que si el controlador entra en una falla irrecuperable, este se debe anular completamente y pasar a un modo de operación manual, similar a como se realiza actualmente.

El primer cambio importante se hará en la \textit{etapa de potencia} debido a que los sopladores están conectados directamente a los contactores de arranque. El sistema de control es capaz de variar la velocidad del motor para ajustar el caudal de aire soplado, es por esto que se incorporará un variador de frecuencia y un par de contactores de enclavamiento a la entrada del mismo.

En paralelo a los sistemas digitales que actualmente operan (PLC, HMI, SCADA, etc.), el controlador tendrá su propia interfaz de visualización y ajuste, en donde se efectuarán todas las tareas a cargo del operario.

\section{Análisis económico inicial}

Para disponer de un análisis inicial y tener un panorama general de los beneficios económicos del proyecto, realizamos una estimación del ahorro energético generado por el sistema de control. En este apartado solo se expondrán los cálculos directamente relacionados con el ahorro energético. Puede ver el anexo \ref{anexo:calculoEconomicoInicial} para más información sobre la metodología empleada en este análisis.

Si los sopladores se encuentran funcionando al 100\% las 24 horas del día, y su potencia nominal es de 75HP, o lo que es igual a 55.93kW. Podemos calcular el consumo en kWh en un mes, que se verá reflejado directamente en la tarifa eléctrica.

El costo de la energía se compone de 2 facturas, la primera es el costo de la energía en el mercado mayorista (CAMMESA) y la segunda factura es el costo del transporte de la Energía, en este caso ENERSA es quien se encarga de vincular al \textbf{usuario mayorista} con el sistema interconectado (SADI), por lo tanto, es quien cobra una tarifa de peaje.

Puntualmente, no se pudieron tener los datos del contrato particular de la industrial acerca de la compra en el mercado mayorista, pero como referencia hay un cuadro de la resolución de la secretaria de energía, donde figura el costo de la energía y potencia para los grandes usuarios conectados a las distribuidoras (el costo final debería ser un ligeramente menor a ese precio).

Al día 17 de mayo de 2022, el costo del MWh según la resolución \href{https://microfe.cammesa.com/static-content/CammesaWeb/download-manager-files/Mem/RESOL-2022-305-APN-SE-MEC.pdf}{RESOL-2022-305-APN-SE de CAMMESA}, que abarca desde mayo a julio del 2022, es de, 13.675ARS en promedio entre pico, valle y resto. Además, se debe tener en cuenta el \textit{peaje}, a cargo de ENERSA para conectar a la industria con el sistema interconectado (SADI), cuyo costo desconocemos, pero compensamos con el cuadro tarifario de CAMMESA.

\begin{figure}[H]
    \centering
    \includegraphics[scale=.5]{src/imagenes/economia/costoCammesa.png}
\end{figure}

Si la potencia eléctrica de los motores es de 55,93kW, eso significa un consumo de 55,93kWh. Si el rendimiento de los motores en este régimen según la hoja de datos de los Sopladores Repicky R3.0 75HP es del 92,7\%, entonces el consumo será de 60,3kWh aproximadamente.

Tomando un escenario conservador, si se logra una mejora en la eficiencia cercana al 30\%, lo que implicaría un ahorro de 18,1kWh o de 180.687,8ARS por mes. Si sumamos el costo, por año se traduce en 2.168.253ARS o en 18.492USD\footnote{Precio dólar a 18 de mayo de 2022 es de \href{https://www.cronista.com/finanzas-mercados/dolar-hoy-a-cuanto-cotiza-en-los-bancos-de-la-city-este-miercoles-18-de-mayo/}{117,25ARS}.}. \textbf{Mas de 2 millones de pesos o casi 19 mil dólares anuales.}

Con base a lo expuesto se puede decir que incorporar un sistema de control para los sopladores en la pileta de aireación, produce un ahorro de costos del proceso y una mejora en las condiciones de operación para la instalación.